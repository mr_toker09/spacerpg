﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InteractableController : MonoBehaviour
{
    public string actionText;
    public Text actionTextField;

    public bool playerInRange = false;
    public BoolValue interactionPermission;

    [HideInInspector]
    public ShipManeuverController playerShip;

    private void OnValidate()
    {
        playerShip = FindObjectOfType<ShipManeuverController>();
    }

    IEnumerator TypeActionText()
    {
        ResetActionText();
        foreach (char letter in actionText.ToCharArray())
        {
            actionTextField.text += letter;
            yield return null;
        }
    }

    public void ResetActionText()
    {
        actionTextField.text = "";
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!interactionPermission.RuntimeValue)
            return;

        if (collision.gameObject.tag == "Player")
        {
            playerInRange = true;
            StartCoroutine(TypeActionText());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (!interactionPermission.RuntimeValue)
            return;

        if (collision.gameObject.tag == "Player")
        {
            playerInRange = false;
            StopCoroutine(TypeActionText());
            ResetActionText();
        }
    }
}
