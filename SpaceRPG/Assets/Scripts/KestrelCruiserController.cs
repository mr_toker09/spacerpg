﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KestrelCruiserController : InteractableController
{
    public Image salvageBar;
    public float timeToCollect;
    public FloatValue salvageCargo;

    private void OnEnable()
    {
        ResetActionText();
        salvageBar.gameObject.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (playerInRange)
        {
            if (Input.GetButton("Interact"))
            {
                salvageBar.gameObject.SetActive(true);

                if (salvageCargo.RuntimeValue > 0)
                {
                    salvageCargo.RuntimeValue -= timeToCollect;
                    playerShip.totalScrap.RuntimeValue += timeToCollect;
                    
                }
                else
                {
                    interactionPermission.RuntimeValue = false;
                    ResetActionText();
                }
                salvageBar.fillAmount = salvageCargo.RuntimeValue / salvageCargo.initialValue;
            }
        }
        else
        {
            salvageBar.gameObject.SetActive(false);
        }
    }
}
